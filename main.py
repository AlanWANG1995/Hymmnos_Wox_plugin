# -*- coding: utf-8 -*-

from wox import WoxAPI, Wox
from sql import HymmnosDict

class HymmnosPlugin(Wox):

    def query(self, query):
        # res = self.db.find(query)
        db = HymmnosDict()
        res = db.find(query)
        results = [{
            "Title": i,
            "SubTitle": "Meaning: %s, Type: %s, Note: %s" % ( e, o, t),
            "IcoPath": "Images/hymmnos.png"
        } for i, e, o, t in res]

        return results
    

if __name__ == "__main__":
    HymmnosPlugin()